// ==== Problem #1 ====
// The owner of the company wants to find the details of the user with id 100. Help the dealer find out which user has an id of 100 by calling a function that will return the data for that user. Then log the car's name, email, and gender in the console log in the format of: 
// "*User name goes here* is a *user gender goes here* and can be contacted on *user email goes here*"





function findInformation(userList, id) {

    if (userList == null || id == null || !(typeof id == "number") || !(Array.isArray(userList)) || userList.length == 0) {
        return [];
    } else {
        for (let i = 0; i < userList.length; i++) {
            if (userList[i].id == id) {
                return userList[i];
            }
        }
        return "The id is not in the list."
    }
}




module.exports = findInformation;
