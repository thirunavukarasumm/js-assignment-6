const problem6Test = require('../problem6')

const data = require('./dataset.js')

const resultArray = problem6Test(data);

if (resultArray.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultArray);
}
