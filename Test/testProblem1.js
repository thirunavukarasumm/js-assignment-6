const problem1Test = require('../problem1.js')

const data = require('./dataset.js')

const resultObject = problem1Test(data, 100);

if (resultObject.length == 0) {
    console.log("Invalid data was entered");
} else if (typeof resultObject == "string") {
    console.log(resultObject);
} else {
    console.log(`${resultObject.first_name + resultObject.last_name} is a ${resultObject.gender} and can be contacted on ${resultObject.email}.`);
}

