const problem2Test = require('../problem2')

const data = require('./dataset.js')

const resultObject = problem2Test(data);

if (resultObject.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(`Last user is a ${resultObject.first_name + resultObject.last_name} and can be contacted on ${resultObject.email}.`);
}
