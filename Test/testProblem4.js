const problem4Test = require('../problem4')

const data = require('./dataset.js')

const resultArray = problem4Test(data);

if (resultArray.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultArray);
}
