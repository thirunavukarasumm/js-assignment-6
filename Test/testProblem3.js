const problem3Test = require('../problem3')

const data = require('./dataset.js')

const resultArray = problem3Test(data);

if (resultArray.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultArray);
}
