// ==== Problem #3 ====
// The marketing team wants the users listed alphabetically on the website by last name. Execute a function to Sort all the user's names into alphabetical order and log the results in the console as it was returned.


function userLastNameSorted(userList) {
    if (userList == null || !(Array.isArray(userList)) || userList.length == 0) {
        return [];
    } else {
        return userList.sort((a, b) => a.last_name.localeCompare(b.last_name));
    }

}



module.exports = userLastNameSorted;
