// ==== Problem #5 ====
// The marketing manager needs to find out how many users are male. Find out how many users are male and return the array of names and emails and log their length.

function onlyMaleUsers(userList) {

    if (userList == null || !(Array.isArray(userList)) || userList.length == 0) {
        return [];

    } else {

        let resultListWithEveryMaleUser = [];
        for (let i = 0; i < userList.length; i++) {
            if (userList[i].gender === "Male") {
                resultListWithEveryMaleUser.push({
                    first_name: userList[i].first_name,
                    last_name: userList[i].last_name,
                    email: userList[i].email
                });
            }
        }

        return resultListWithEveryMaleUser;
    }


}

module.exports = onlyMaleUsers;

