// ==== Problem #6 ====
// The metrics team is interested in seeing how many users fall into which gender.  Execute a function and return an array of arrays where each array contains the user information of a particular gender in the order Male, Female, Polygender, Bigender, Genderqueer, Genderfluid, Agender.  Once you have the array, use JSON.stringify() to show the results of the array in the console.

let userOrder = {
    Male: 0,
    Female: 1,
    Polygender: 2,
    Bigender: 3,
    Genderqueer: 4,
    Genderfluid: 5,
    Agender: 6,
    "Non-binary": 7 // The gender which was left out in the question.
}

function userFilter(userList) {

    if (userList == null || !(Array.isArray(userList)) || userList.length == 0) {
        return [];
    } else {
        let userFilteredList = [];
        for (let i = 0; i < userList.length; i++) {
            if (userFilteredList[userOrder[userList[i].gender]] === undefined) {
                userFilteredList[userOrder[userList[i].gender]] = [userList[i]]
            } else {
                userFilteredList[userOrder[userList[i].gender]].push(userList[i])
            }

        }
        return userFilteredList;
    }
}


module.exports = userFilter;
