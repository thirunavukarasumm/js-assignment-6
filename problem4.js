// ==== Problem #4 ====
// The marketing team needs all the emails from every user on the list. Execute a function that will return an array from the user data containing only the emails and log the result in the console as it was returned.


function getEmailOfEveryUsers(UserList) {
    if (UserList == null || !(Array.isArray(UserList)) || UserList.length == 0) {
        return [];
    } else {
        let emailList = [];
        for (let i = 0; i < UserList.length; i++) {
      
            emailList.push(UserList[i].email);
        }
        return emailList;
    }
}

module.exports = getEmailOfEveryUsers;
