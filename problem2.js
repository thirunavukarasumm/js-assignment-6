// ==== Problem #2 ====
// The owner of the company wants the information on the last user in the list. Execute a function to find what the name and email of the last user in the inventory is.  Log the name and email into the console in the format of: 
// "Last user is *user name goes here* and can be contacted on *user email goes here*"


function findInformationOfLastUser(userList) {
    if (userList == null || !(Array.isArray(userList)) || userList.length == 0) {
        return [];
    } else {
        let index = userList.length - 1
        return userList[index];
    }

}



module.exports = findInformationOfLastUser;
